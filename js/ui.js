function createUI(){
    main_interface = webix.ui(
    {
        id: "main",
        rows: [
            { view: "toolbar", padding:3, elements: [
                { view: "button", type: "icon", icon: "bars",
                width: 30, align: "center", css: "app_button", click: function(){
                $$("left_menu").toggle();
            }
        },
        { view: "label", type: "button", label: "ImageRR"},
        {},
        { view: "button", type: "icon", width: 45, css: "app_button", icon: "bell-o"}
]
},
{ 
    cols: [{
            view: "sidebar",
            id: "left_menu",
            width: 150,
            data: menu_data,
            on: { onAfterSelect: function(id){ menuChoise(id); } }
        }, {
        rows:[ 
            { id: "profile_panel", rows:[ profile_header, profile_menu, { view: "scrollview", body: photo_grid }] },
            { id: "sub_panel", rows:[ { rows:[ { view: "label", label: "Мои подписки" }, 
                                      { height: 230, view: "scrollview", body: subs_grid }]}, 
                            { cols: [ { view: "scrollview", body: subs_photo_grid }] }  ]},
            { borderless: true, id: "albums_panel", rows:[ albums_menu, { view: "scrollview", body: albums_grid } ] },
            { id: "albums_photos_panel", rows:[ albums_photos_menu, { view: "scrollview", body: album_photos_grid } ] },
            { id: "image_panel", rows:[{
                rows:[
                    { height: 30 },
                    { cols:[
                        {},
                        { width: 960, height: 600, template: function(){ return "<canvas id='re_picture' width='960' height='600'></canvas>"; } },
                        {}
                    ] },
                    { height: 30, cols: [
                        {},
                        { width: 90, view: "label", label: "Эффекты" },
                        { width: 120, view: "button", value: "Контраст", click: 'image_filter(redact_image, "con");' },
                        { width: 120, view: "button", value: "Сепия", click: 'image_filter(redact_image, "se");' },
                        { width: 120, view: "button", value: "Затенение", click: 'image_filter(redact_image, "br");' },
                        { width: 120, view: "button", value: "Насыщение", click: 'image_filter(redact_image, "sut");' },
                        {}
                    ] }
                ]
            }]},
            { id: "options_panel", rows:[panel_settings,{}]},
            { id: "upload_panel", view: "layout", type: "space", rows: [ { }, uploadBox, { } ] }
           ]
        }		
    ]
}
]});
}