var upload_count = 0;
var list_count = 0;
var ins_ids = [];
var redact_image = {};

function filesSend() {
		$$("send_files").define("formData", { album: $$("album_selector").getValue() });
		$$("send_files").refresh();
		$$("send_files").send(function(resp) { });

}

function img(obj){
	return '<img width="640" height="480" src="'+obj.src+
		   '" class="content" ondragstart="return false"/>'
}

function createViewImageWindow(image){
	webix.ui({
		view:"window",
		id:"show_photo_full",
		height: 600,
		width: 960,
		modal:true,
		position:"center",
		head: { view: "toolbar",
				height: 35,
				cols:[
					{ view: "label", label: "Просмотр изображения"},
					{ view:"button", width: 25, value: "X", align:"right", 
					  click: "$$('show_photo_full').close();"}
				] },
		body:{
			cols:[
				{},
				{ 	
					rows:[{},
					{	height: 600,
						width: 960,
						template: function(){
							let _width = "";
							let _height = "";
							var img = new Image();
							img.src = image;

							img.onload = function() {
							 _width = (this.width > 960)? "85%": "";
							 _height= (this.height > 600)? "85%": "";
							 _image = document.getElementById("img_full");
							 _image.style.width = _width;
							 _image.style.height = _height;
						}
						return '<img style="margin: auto; position: absolute;' + 
								'top: 0; left: 0; bottom: 0; right: 0;' + _width + _height +'" src="'+ image +
								'" id="img_full" ondragstart="return false"/>';}
					},{}
				]},
				{}
			]
		}
	}).show();
}

function createComment(nick, comment, date) {
	return {
			view: "form",
	  		borderless: false,
	  		autoheight:true,
			width: 320,
			rows:[
			{ cols:[
				{ padding: 3, align: "left", borderless: false, view: "label", label: nick },
				{ padding: 3, align: "right", borderless: false, view: "label", label: date }
			] },
			{ padding: 3, borderless: false, autoheight:true, template: comment },
		]
}

}

function createViewImageWindowWithComments(image, _id){
	webix.ui({
		view:"window",
		id:"show_photo_comments",
		height: 600,
		width: 960,
		modal:true,
		position:"center",
		head: { view: "toolbar",
				height: 35,
				cols:[
					{ view: "label", label: "Просмотр изображения"},
					{ view:"button", width: 25, value: "X", align:"right", 
					  click: "$$('show_photo_comments').close();"}
				] },
		body:{
			rows:[
				{},
				{ 	
					cols:[
					{	height: 500,
						css: ".content-centering",
						template: function(){
						return '<img style="max-width: 100%;" ' + 'src="'+ image +
								'" id="img_full" ondragstart="return false"/>';}
					},{
						rows:[{ 
								view: "scrollview",
								scroll:"y",
								id: "comment_scroll",
							    body: {
									margin: 15, 
									padding: 2 , 
									type:"wide", 
									view:"flexlayout",
									id: "comments_grid",
									cols: []
							} 
						},
							{
								view: "form", 
								height: 60,
								padding: 5,
								width: 360,
								cols:[
									{ view: "textarea", id: "comment_area" },
									{ view: "button", width: 90, value: "Отправить",
								      click: function() {
										  val = $$("comment_area").getValue();
										  		$$("comment_area").setValue("");
										  $$("comments_grid").addView(createComment(getCookie("nick"), val, 
										  					dateFormat(new Date(), 'Y-m-d')));
										  $$("comment_scroll").scrollTo(0, 2000);
										  add_comment(_id, getCookie("user_id"), val);
									}
								}]
							}
						]
					 }
				]},
				{}
			]
		}
	}).show();
	load_comments(_id);
	show_progress_bar(500);
}

function deletePhoto(_photo, _panel) {
	webix.ajax().post('/photo/delete', { photo: _photo });
	$$(_panel).removeView(_photo);
}

function add_comment(_photo, _user, _comment) {
console.log({ photo: _photo, user: _user, comment: _comment});
	webix.ajax().post('/main/add_comment', { photo: _photo, user: _user, comment: _comment}, 
	function(data){

	});
}

function load_comments(_photo) {
	webix.ajax().post('/main/get_comments', { photo: _photo}, function(data){
				comments = JSON.parse(data);
						
				for(let comment in comments) $$("comments_grid").addView(createComment(
												comments[comment].nick, 
												comments[comment].comment, 
												comments[comment].add_date));
				$$("comment_scroll").scrollTo(0, 2000);
	});	
}

function createExportImageWindow(image) {
	webix.ui({
		view:"window",
		id:"export_win",
		height: 180,
		width: 600,
		modal:true,
		position:"center",
		head: { view: "toolbar",
				height: 35,
				cols:[
					{ view: "label", label: "Получить ссылку на изображение"},
					{ view:"button", width: 25, value: "X", align:"right", 
					  click: "$$('export_win').close();"}
				] },
		body:{
			padding: 20,
			rows:[
				{},
				{ view:"text", label:"Прямая ссылка: ", value: image, labelWidth: 120 },
				{ view:"text", label:"HTML код: ", value: '<img src="'+ image + '"/>', labelWidth: 120 },
				{}
			]
		}
	}).show();	
}

function image_filter(image, filter) {
	document.querySelector('#re_picture').removeAttribute('data-caman-id'); 
	Caman("#re_picture", image, function () {
		if(this.width > 960 && this.height > 600)
		{
			this.resize({
				width: 960,
				height: 600
			  });
		}
		
		switch(filter)
			  {
				case "br": this.brightness(10); break;
				case "con": this.contrast(30); break;
				case "se": this.sepia(60); break;
				case "sut": this.saturation(-30); break;
			  }

			  this.render();
	  });
}

function newPicbox(picture, header, _min, _max, _id, _panel) {
	return {
		 id: _id, borderless: false, rows:[
			{ type:"layout", rows: [
				{ template: header, height: 28, borderless: true },
				{ view:"button", type: "image", image:"/imgs/"+ picture, width: _min, height: _min, 
				  maxWidth: _max, maxHeight: _max,  
				  click: 'createViewImageWindow("/imgs/'+ picture + '");'},
				{ padding: 5,
					 cols:[ { view: "button", type: "icon", label: "", icon: "trash", width: 30, 
					          click: 'deletePhoto(' +_id + ',"' + _panel +'")' },
					 { view: "button", type: "icon", label: "", icon: "external-link", width: 30, 
					 click: 'createExportImageWindow("' + 'http://imagerr.ua/imgs/' + picture +'")'},
					 { view: "button", type: "icon", label: "", icon: "edit", width: 30, 
					 click: function(){
						$$("left_menu").select("image");
						redact_image = '/imgs/'+ picture;
						document.querySelector('#re_picture').removeAttribute('data-caman-id'); 
						Caman("#re_picture", '/imgs/'+ picture, function () {
							if(this.width > 960 && this.height > 600)
							{
								this.resize({
									width: 960,
									height: 600
								  });
								  redact_image = this;
								  this.render();
							}
						  });
					 }
					},
					 {},
					 { view: "button", type: "icon", label: "", icon: "download", width: 30, 
					 click: 'location.href = "' + 'http://imagerr.ua/imgs/' + picture +'"'},
					  { view: "button", type: "icon", label: "", icon: "eye", width: 30, 
					 click: 'createViewImageWindow("/imgs/'+ picture + '");'}

				] }
			]},
		]
	}
};

function newCommentPicbox(picture, header, _min, _max, _id, _panel) {
	return {
		 id: _id, borderless: false, rows:[
			{ type:"layout", rows: [
				{ template: header, height: 28, borderless: true },
				{ view:"button", type: "image", image:"/imgs/"+ picture, width: _min, height: _min, 
				  maxWidth: _max, maxHeight: _max,  
				  click: 'createViewImageWindowWithComments("/imgs/'+ picture + '",' + _id + ');'},
				{ padding: 5,
					 cols:[ { view: "button", type: "icon", label: "", icon: "trash", width: 30, 
					          click: 'deletePhoto(' +_id + ',"' + _panel +'")' },
					 { view: "button", type: "icon", label: "", icon: "external-link", width: 30, 
					 click: 'createExportImageWindow("' + 'http://imagerr.ua/imgs/' + picture +'")'},
					 { view: "button", type: "icon", label: "", icon: "edit", width: 30,
					   click: function() {
						$$("left_menu").select("image"); 
						Caman("#re_picture", '/imgs/'+ picture, function () {
							if(this.width > 960 && this.height > 600)
							{
								this.resize({
									width: 960,
									height: 600
								  });
								  this.render();
								  redact_image = this;
							}
						  });
					 }
					},
					 {},
					 { view: "button", type: "icon", label: "", icon: "download", width: 30, 
					 click: 'location.href = "' + 'http://imagerr.ua/imgs/' + picture +'"'},
					  { view: "button", type: "icon", label: "", icon: "eye", width: 30, 
					 click: 'createViewImageWindowWithComments("/imgs/'+ picture + '",' + _id + ');'}

				] }
			]},
		]
	}
};

function newAlbum(header, _min, _max, _maxHeight, album) {
	return {
		minWidth: _min, minHeight: _min, maxWidth: _max, maxHeight: _maxHeight, rows:[
			{ type:"layout", rows: [
				{ template: header, height: 28 },
				{ view:"button", type: "image", image:"/imgs/album.png", width: 200, height: 200, 
				  click: "showAlbumPhoto(" + album + ")"},
			]}
		]
	}
};

function newUserPic(header, userAva, _id) {
	return {
		minWidth: 195, minHeight: 205, maxWidth: 195, maxHeight: 200, padding: 10,  rows:[
			{ type:"layout", rows: [
				{ template: header, borderless: true, height: 28 },
				{ view:"button", type: "image", image:"/imgs/"+ userAva, width: 190, height: 190,
			click: 'load_public_album_photos('+ _id +');'}
			]}
		]
	}
};

function loadSubs(_user)
{
	clearMultiview("subs_grid");
	show_progress_bar(500);
	webix.ajax().post('/main/get_subs', { user: _user },
				    function(data){
						subs = JSON.parse(data);
						
						for(let sub in subs)
						{
							$$("subs_grid").addView(newUserPic(subs[sub].nick, 
															   subs[sub].avatar, 
															   subs[sub].subscribe_id));
						}
				    });
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setup(id) {
	currentElement.hide(); 
	currentElement = $$(id); 
	currentElement.show();
}

function submit(){
    webix.ajax().post('/main/authorize', $$("login_form").getValues(), 
    function(data){
            var us = JSON.parse(data);
            if( us.status != "error") { 
                login_register.hide(); 
				main_interface.show();
				$$("left_menu").select("profile"); 
				show_progress_bar(1000);
				
                document.cookie = "user=true";
                document.cookie = "user_id=" + us.id;
                document.cookie = "nick=" + us.nick;
                document.cookie = "avatar=" + us.avatar;

                let display = (us.nick == us.name)? us.name: us.nick; 

                $$("username").define("label", display);
                $$("username").refresh();

                $$("picbar").define("label", "Изображения " + display);
                $$("picbar").refresh();
                
                $$("avatar").define("image", "/imgs/" + us.avatar);
				$$("avatar").refresh();
            }
            else { 
                login_register.show(); 
                main_interface.hide(); 
                webix.alert("Не удалось войти в систему."+ 
                               " Проверьте правильность введённых данных"); 
            }
    });
}

function submitR(){
    webix.ajax().post('/main/register', $$("reg_form").getValues(), function(data){ webix.alert(data);});
}

function load_last_photo(id)
{
	//clearMultiview("photo_grid");
	show_progress_bar(500);
	webix.ajax().post('/photo/download', { user: id, action: "get_last" },
	
				    function(data){
                        last_user_photos = JSON.parse(data);
                        
						for(let photo in last_user_photos)
						{
							$$("photo_grid").addView(newPicbox(last_user_photos[photo].file_path, 
															   last_user_photos[photo].input_name, 300, 500, 
															   last_user_photos[photo].photo_id, 
															   "photo_grid"));
						}
				    });
}

function load_albums(id) {
	show_progress_bar(500);
	webix.ajax().post('/album/get_albums', { user_id: id }, 
	function(data){ 
		albums = JSON.parse(data);

		for(let album in albums) $$("albums_grid").addView(newAlbum(albums[album].name, 
						 						   180, 210, 210, albums[album].album_id)); 
	});
}

function load_public_album_photos(_user) {
	clearMultiview("subs_photo_grid");
	show_progress_bar(500);
	webix.ajax().post('/photo/download', { user: _user, action: "get_public" },
				    function(data) {
						public_album_photos = JSON.parse(data);
						
						for(let photo in public_album_photos)
						{
							$$("subs_photo_grid").addView(newCommentPicbox(public_album_photos[photo].file_path, 
															public_album_photos[photo].input_name, 300, 500,
															public_album_photos[photo].photo_id, 
															"subs_photo_grid"));
						}
				    });
}

function load_albums_opts(id) {
	show_progress_bar(300);
	webix.ajax().post('/album/get_albums', { user_id: id }, 
	function(data){ 
		albums = JSON.parse(data);

		let opts = [];
		for(let album in albums) opts.push({ id: albums[album].album_id, 
											 value: albums[album].name });

		$$("album_selector").define("options", opts);
		$$("album_selector").refresh();
	});
}

function showAlbumPhoto(_album) {
	clearMultiview("albums_photos_grid");
	show_progress_bar(500);
	webix.ajax().post('/photo/download', { album: _album, action: "get_album" },
				    function(data){
						album_photos = JSON.parse(data);
						
						for(let photo in album_photos)
						{
							$$("albums_photos_grid").addView(newPicbox(album_photos[photo].file_path, 
															   album_photos[photo].input_name, 300, 500,
															   album_photos[photo].photo_id, 
															   "albums_photos_grid"));
						}
				    });
	$$("albums_photos_panel").show();
	$$("albums_panel").hide();
	
}

function clearMultiview(mult) {
	let childs = $$(mult).getChildViews();

	let length = childs.length;
	let ids = [];

	for(let i = 0; i < length; i++) 
		ids[i] = childs[i].config.id;

	for(let i = 0; i < length; i++) {
			$$(mult).removeView(ids[i]);
	}

}

function menuChoise(id) {
	clearMultiview("photo_grid");
	clearMultiview("albums_photos_grid");
	clearMultiview("albums_grid");
	clearMultiview("subs_photo_grid");
	$$("albums_photos_panel").hide();
	switch(id){
            case "exit": 
                webix.ajax().post('/main/logout', {});
                	document.cookie = "user=false";
                	document.cookie = "menu=profile";
					location.reload();
					break;
			case "profile": setup("profile_panel"); load_last_photo(getCookie("user_id")); break;
			case "sub": setup("sub_panel"); loadSubs(getCookie("user_id")); break;
			case "albums": setup("albums_panel"); load_albums(getCookie("user_id")); break;
			case "upload": setup("upload_panel"); load_albums_opts(getCookie("user_id")); break;
			case "image": setup("image_panel"); break;
			case "options": setup("options_panel"); break;
	}
	if(id != "exit")document.cookie = "menu="+id;
}

function show_progress_bar(delay) { 
	$$("main").disable();
	$$("main").showProgress({
	  type:"top",
	  delay:delay,
	  hide:true
	});
	setTimeout(function(){
	  $$("main").enable();
	}, delay);
  };
