var login_register = {};
var user = {};
var pic = 1;

var pic_count = 6;

setInterval(function(){  
  if ($$("pictures").getActiveIndex() < pic_count-1)
    $$("pictures").showNext();
  else $$("pictures").setActiveIndex(0);        
}, 3000);

var pics = [
		{ template:img, data:{ src:"imgs/1.jpg" } },
		{ template:img, data:{ src:"imgs/2.jpg" } },
		{ template:img, data:{ src:"imgs/3.jpg" } },
		{ template:img, data:{ src:"imgs/4.jpg" } },
		{ template:img, data:{ src:"imgs/5.jpg" } },
		{ template:img, data:{ src:"imgs/6.jpg" } }
]

function img(obj){
    return '<img width="600" height="400" src="'+obj.src+'" class="content" ondragstart="return false"/>'
}

webix.ready(function() { 
	login_register = webix.ui({    
         id: "login_reg",
         view: "layout",
		 rows: [ 
            { view: "toolbar", 
              padding: 3, 
              elements: [ { view: "label", 
                            type: "button", 
                            label: "ImageRR - сервис для хранения изображений"} 
                        ] },
            { height: 100, 
              rows:[{ height: 30},
                    { id: "welcome", 
                      view: "label",  
                      size: 36,  
                      align: "center", 
                      label: "Войдите или зарегистрируйтесь прямо сейчас, " +
							 "что бы загрузить фотографии и поделится ими со всем миром!"}]},
			{ cols:[
				{ width: 140 },{
					view:"form",
					borderless: false,
					width: 500,
					height: 372,
					elements: [
								{ view:"tabview", borderless: true, cells:[
									{ header: "Вход",
										body:{
											view:"form",
											id:"login_form",
											papdding: 10, 
											rows:[ { height: 20 },
												{ view:"text", label:"Пользователь", name:"username", labelWidth: 120 },
												{ view:"text", label:"Пароль", name:"password", type:"password", labelWidth: 120},
												{ view:"checkbox", labelRight:"Оставаться в системе", name:"accept" },
												{ view:"button", value: "Вход", width: 150, align:"center", click:submit }
											  ]
										   }
										},
								{ header: "Регистрация",
										body:{
											id:"reg_form",
											view:"form",
											papdding: 10, 
											rows:[ { height: 20 },
												{ view:"text", label:"Пользователь", name:"username", labelWidth: 120 },
												{ view:"text", label:"Email", name:"email", labelWidth: 120 },
												{ view:"text", label:"Пароль", name:"password", type:"password", labelWidth: 120 },
												{ view:"text", label:"Повторить", name:"password_retype", type:"password", labelWidth: 120 },
												{ view:"checkbox", labelRight:"Я согласен с условиями сервиса", name:"accept_terms" },
												{ view:"button", value: "Регистрация", width: 150, align:"center", click:submitR }
											]
										}
									}
								]}
							]
						},{},
					{
						view:"form",
						type:"wide",
						rows:[{
							view:"carousel",
							id:"pictures",
							width: 610,
							height: 410,
							borderless: true,
							navigation:{
								type: "side",
								items: true
							},
							cols: pics,
				}],
			},{ width: 150 }]
		},{}]
    });
});
