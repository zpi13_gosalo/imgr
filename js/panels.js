var menu_data = [
	{id: "profile", icon: "user", value:"Профиль"},
	{id: "sub", icon: "plus", value:"Подписки"},
	{id: "albums", icon: "book", value: "Альбомы"},
	{id: "upload", icon: "upload", value:"Загрузка"},
	{id: "image", icon: "pencil-square-o", value:"Редактор"},
	{id: "options", icon: "gear", value:"Настройки"},
	{id: "exit", icon: "sign-out", value:"Выход"}
];

var ph_grid = [];
  var photo_grid = {
	margin: 15, 
	padding: 2 , 
	type:"wide", 
	view:"flexlayout",
	id: "photo_grid",
	cols: ph_grid
};


var subs_ph_grid = [];
var subs_photo_grid = {
	margin: 15, 
	padding: 2 , 
	type:"wide", 
	view:"flexlayout",
	id: "subs_photo_grid",
	cols: subs_ph_grid
};


var subs_users_grid = [];
var subs_grid = {
	margin: 15,
	padding: 2 , 
	type:"wide", 
	view:"flexlayout",
	id: "subs_grid",
	cols: subs_users_grid
};


var alb_ph_grid = [];
var album_photos_grid = {
	margin: 15, 
	padding: 2 , 
	type:"wide", 
	view:"flexlayout",
	id: "albums_photos_grid",
	cols: alb_ph_grid
};

var alb_grid = [];
var albums_grid = {
	margin: 15, 
	padding: 2 , 
	type:"wide", 
	view:"flexlayout",
	id: "albums_grid",
	cols: alb_grid
};

var uploadBox = { 
	view: "layout",
	id: "upload_form",
	padding: 7,
	rows:[
	{	
		view: "uploader", 
		id:"send_files",
		link: "upload_list",
		autosend: false, 
		upload:"/photo/upload",
		on:{
			onBeforeFileAdd: function(item){
			  var type = item.type.toLowerCase();
			  if (type != "jpg" && type != "png"){
				webix.alert("Неверный формат файлов");
				return false;
			  }
			},
			onFileUpload: function(item){
			},
			onFileUploadError: function(item){
			  webix.alert("Ошибка при загрузке файлов");
			}
		} 
	},
	{
		type: "space",
		cols:[
			{},{ 
				view:"select", 
				label:"Альбом",
				id: "album_selector", 
				options:[ 
				],
				labelAlign:'right',
				width: 320 
			},{}
		]
	},
	{
		type: "space",
		rows: [
			{
				cols:[
					{},{ view:"list", id:"upload_list", type:"uploader", width:500, height: 250 },{}
				]
			}, { height: 15 },
			{
				cols:[
					{},{ view: "button", label: "Выбрать файл", 
						 width: 155, 
						 click: function(){ $$("send_files").fileDialog(); }
						},
						{ view: "button", label: "Загрузить", 
						width: 155, 
						click: function(){ filesSend(); }
					    },{}
				]
			},
	], 
	}
]};


var profile_header = {
	padding: 7, id: "head",
	height: 172,
	cols: [
		{ 
			responsive: "head", 
			width: 150, 
            height: 150,
            id: "avatar",
            view: "button",
            type: "image",
            image: "/imgs/noava.jpg",
        },
		{
			padding: 7,
			rows: [
				{ cols:[{ id:"username", view: "label", label: "none", height: 30, width: 100 }, 
						
					] },
				{ view: "label", label: "3 ПОДПИСАН", height: 30, width: 120},
				{ view: "label", label: "0 ПОДПИСЧИКА", height: 30, width: 120},
				{ view: "button", label: "Редактировать", width: 150, click: '$$("change_profile").show();'}
			]
		},{}
	], view: "layout"	
};

var profile_menu = { 
	view: "layout", 
	padding: 7, 
	cols:[
		{ id: "picbar", view: "label", label: "Изображения none", height: 50, width: 150 },
		{ view: "button", label: "Новые", height: 50, width: 100 },
		{ view: "button", label: "Старые", height: 50, width: 110 },
		{},
		{ view: "button", label: "Выделить всё", height: 50, width: 130 },
		{ view: "button", label: "Действия", height: 50, width: 110 },
	],
	height: 50 
}

var albums_menu = { 
	view: "layout",
	padding: 7,  
	cols:[
		{ view: "label", label: "Список альбомов", height: 50, width: 130 },
		{ view: "button", label: "Создать", height: 50, width: 100, click: "$$('create_album').show()" },
		{},
		{ view: "button", label: "Выделить всё", height: 50, width: 130 },
		{ view: "button", label: "Действия", height: 50, width: 110 }
	], 
	height: 50 
}

var albums_photos_menu = { 
	view: "layout",
	padding: 7,  
	cols:[
		{ view: "label", label: "Изображения в альбоме", height: 50, width: 180 },
		{ view: "button", label: "Назад", height: 50, width: 100, click: function(){
									$$("albums_photos_panel").hide();
									$$("albums_panel").show();
		} },
		{},
		{ view: "button", label: "Выделить всё", height: 50, width: 130 },
		{ view: "button", label: "Действия", height: 50, width: 110 }
	], 
	height: 50 
}

function changeTheme(val){ 
    document.getElementsByTagName("link")[0].href = 
    "/css/skins/" + $$(val).getValue() + ".css";
}

var panel_settings = {
    view: "layout",
	padding: 10,  
	rows: [
        { 
            view:"label", 
				label:"Тема/цветовая схема интерфейса:",
                align:'left',
                width: 250,
        },
        { view: "button", value: "flat", click: changeTheme},
        { view: "button", value: "clouds", click: changeTheme},
        { view: "button", value: "compact", click: changeTheme},
        { view: "button", value: "glamour", click: changeTheme},
        { view: "button", value: "light", click: changeTheme},
        { view: "button", value: "metro", click: changeTheme},
        { view: "button", value: "terrace", click: changeTheme},
        { view: "button", value: "web", click: changeTheme},
	]
}