function createWindows(){
        webix.ui({
            view:"window",
            id:"change_profile",
            height: 450,
            head: { 
                view: "toolbar",
                cols: [
                    { view: "label", label: "Изменить информацию профиля"},
                    { view:"button", value: "X", width: 30, align:"right", click: "$$('change_profile').hide();" }
                ]
            }, 
            modal:true,
            position:"center", 
            body:{
                id:"change_user_info",
                view:"form",
                papdding: 10,
                width: 450, 
                rows:[ { height: 20 },
                    { view:"text", label:"Никнейм", name:"nick", labelWidth: 120 },
                    { view:"text", label:"Email", name:"email", labelWidth: 120 },
                    { view:"text", label:"Пароль", name:"password", type:"password", value: "password", labelWidth: 120 },
                    { view:"text", label:"Повторить", name:"password_retype", value: "password", type:"password", labelWidth: 120 },
                    { view:"button", value: "Сохранить", width: 120, align:"center", 
                    click: function(){ 
                            let info = $$("change_user_info").getValues();
                            console.log(info);
                            webix.ajax().post('/main/user_info', 
                            { user: getCookie("user_id"), type: "info", nick: info.nick, email: info.email });

                            if(info.password != "password" && 
                               info.password_retype != "password")
                            webix.ajax().post('/main/user_info', 
                            { user: getCookie("user_id"), type: "password", password: info.password, 
                                                retype: info.password_retype });
                                $$('change_profile').hide();
                    } 
                }
                ]
            }
        });

        webix.ui({
            view:"window",
            id:"create_album",
            height: 450,
            head: { 
                view: "toolbar",
                cols: [
                    { view: "label", label: "Создать альбом"},
                    { view:"button", value: "X", width: 30, align:"right", click: "$$('create_album').hide();" }
                ]
            }, 
            modal:true,
            position:"center", 
            body:{
                id:"create_album_form",
                view:"form",
                papdding: 10,
                width: 450, 
                rows:[ { height: 20 },
                    { view:"text", label:"Название", name:"name", labelWidth: 120 },
                    { view:"checkbox", labelRight:"Публичный альбом", name:"public", labelWidth: 120 },
                    { view:"textarea", label:"Описание", name:"description", labelWidth: 120 },
                    { view:"button", value: "Создать", width: 120, align:"center", click: "$$('create_album').hide();" }
                ]
            }
        });
    }