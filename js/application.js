var main_interface = {};
var albums = [];
var last_user_photo = [];
var currentElement = {};

webix.ready(function(){
	createUI();
	createWindows();
	webix.extend($$("main"), webix.ProgressBar);
	currentElement = $$("profile_panel");

    $$("sub_panel").hide();
    $$("albums_panel").hide();
    $$("image_panel").hide();
    $$("options_panel").hide();
    $$("upload_panel").hide();
	$$("send_files").hide();
	$$("albums_photos_panel").hide();
	$$("send_files").addDropZone($$("upload_list").$view);
});
