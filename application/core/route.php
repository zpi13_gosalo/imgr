<?php
class Route
{
	static function start()
	{
		$current_controller = 'Main';
		$current_action = 'index';

		$adress = explode('?', $_SERVER['REQUEST_URI']);
		$routes = explode('/', $adress[0]);

		if ( !empty($routes[1]) )
		{	
			$current_controller = $routes[1];
		}
		
		if ( !empty($routes[2]) )
		{
			$current_action = $routes[2];
		}

		$model_name = 'Model_main';
		$current_controller = 'Controller_'.$current_controller;
		$current_action = 'action_'.$current_action;


		$model_file = strtolower($model_name).'.php';
		$model_path = "application/models/".$model_file;
		if(file_exists($model_path))
		{
			include "application/models/".$model_file;
		}

		$controller_file = strtolower($current_controller).'.php';
		$controller_path = "application/controllers/".$controller_file;
		if(file_exists($controller_path))
		{
			include "application/controllers/".$controller_file;
		}
		else
		{
			Route::ErrorPage404();
		}
		
		$controller = new $current_controller;
		$action = $current_action;
		
		if(method_exists($controller, $action))
		{
			$controller->$action();
		}
		else
		{
			Route::ErrorPage404();
		}
	
	}
	
	function ErrorPage404()
	{
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
    }
}
?>