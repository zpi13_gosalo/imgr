<?php
 class View
 {
     function generate($content_view, $template_view, $data = null)
     {
         include 'application/views/'.$template_view;
     }

     public static function gen_pages($data, $module)
     {
         $num_pages = ceil($data['rows_count'] / $data['per_page']);
         $cur_page = $data['current_page'];
         $categ = $data['current_cat'];
         
         $categ = ($categ == null)? "" : "&category=$categ";
         for($i = 1; $i <= $num_pages; $i++) {
             if($i == $cur_page) 
             echo "<a id ='focused' 
             href='/$module?page=$i&category=$categ'>$i </a>";
             else echo "<a href='/$module?page=$i$categ'>$i </a>";
         }
     }

     public static function gen_pages_admin($data, $module)
     {
         $num_pages = ceil($data['rows_count'] / $data['per_page']);
         $cur_page = $data['current_page'];
         
         for($i = 1; $i <= $num_pages; $i++) {
             if($i == $cur_page) 
             echo "<a id ='focused' 
             href='?m=$module&page=$i'>$i</a>";
             else echo "<a href='?m=$module&page=$i'>$i</a>";
         }
     }

     public static function gen_menu($active)
     {
        $menu[0] = "main";
        $menu[1] = "gallery";
        $menu[2] = "chat";
        $menu[3] = "guest_book";
        $menu[4] = "files";

        $menu_caption[0] = "Главная";
        $menu_caption[1] = "Галерея";
        $menu_caption[2] = "Чат";
        $menu_caption[3] = "Гостевая книга";
        $menu_caption[4] = "Файлы";

        echo "<nav>";
        echo "<div class = 'navigation'>";
        for($i = 0; $i<5; $i++) {
            if($menu[$i] == $active) echo "<a id ='focused' href=/".$menu[$i].">".$menu_caption[$i]."</a>";
            else echo "<a href=/".$menu[$i].">".$menu_caption[$i]."</a>";
        }
        echo "</div>";
        echo "</nav>";     
     }
 }
?>