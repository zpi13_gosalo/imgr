<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/webix.css" type="text/css" charset="utf-8">
		<script src="js/webix.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/caman.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/dateFormat.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/login_reg.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/windows.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/panels.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/app_func.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/ui.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/application.js" type="text/javascript" charset="utf-8"></script>
		<style>
			.content-centering {
				display:flex;
				justify-content: center; /*центрируем элемент по горизонтали */
    			align-items: center; /* и вертикали */
    			width: 500px; /* задали размеры блоку-родителю */
				height: 500px;
				overflow: hidden; /* если элемент больше блока-родителя – обрезаем лишнее */
			}
		</style>
		<title>ImageRR - Сервис хранения изображений</title>
	</head>
	<body style="background: #f2f4f7;">
		<script type="text/javascript" charset="utf-8">
		 webix.ready(function() {
			if(getCookie("user") == "true") {
				if(getCookie("menu")){
					$$("left_menu").select(getCookie("menu"));
				}
				
				let nick = getCookie("nick");
				let name = getCookie("name");
				let avatar = getCookie("avatar");

				let display = (nick == name)? name: nick; 

				$$("username").define("label", display);
				$$("username").refresh();

				$$("picbar").define("label", "Изображения " + display);
				$$("picbar").refresh();

				$$("avatar").define("image", "/imgs/" + avatar);
				$$("avatar").refresh();

				let id = getCookie("user_id");
				login_register.hide(); 
				main_interface.show();
			} else { 
				login_register.show(); 
				main_interface.hide(); 
			}
		});
	</script>
	</body>
</html>