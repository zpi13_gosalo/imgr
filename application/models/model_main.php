<?php 

class Model_Main extends Model
{
    private $connection = null;

    function __construct() {
        $this->connection = Model::db_connect();
    }

//Авторизація та реєстрація
//--------------------------------------------------------------------------------------------------------------------
    public function try_authorize($login, $password) {
        $sql = "SELECT * FROM users WHERE username = '$login'";
        $result = $this->connection->query($sql);
        $user = $result->fetch_assoc();
        $pass = $user['password'];
        
        return ($password == $pass)? $user : false;
    }

    public function register($username, $password, $email) {
        $sql = "INSERT INTO users (username, password, email, nick, avatar) 
        VALUES ('$username', '$password', '$email', '$username','noava.jpg')";
        
        if($this->connection->query($sql))
        echo "Регистрация успешна! Введите логин и пароль для входа в систему";
        else echo "Регистрация не удалась. Проверьте коррректность введенных данных";
    }
//--------------------------------------------------------------------------------------------------------------------
//Робота з альбомами
//--------------------------------------------------------------------------------------------------------------------
    public function add_photo_to_album($photo, $album) {
        $sql = "INSERT INTO album_photos (album_id, photo_id) 
        VALUES ($album, $photo)";
        $this->connection->query($sql);
        $sql = "UPDATE albums 
        SET update_date = curdate() 
        WHERE id = $album";
        $this->connection->query($sql);
    }

    public function delete_photo_from_album($photo, $album) {
        $sql = "DELETE FROM album_photos WHERE photo_id = $photo AND album_id = $album";
        $this->connection->query($sql);

        $sql = "UPDATE albums 
        SET update_date = curdate() 
        WHERE id = $album";
        $this->connection->query($sql);
    }

    public function create_album($user, $album_info, $public) {
        extract($album_info);
        $p = ($public) ? 1 : 0;
        $sql = "INSERT INTO albums (name, description, creation_date, update_date, public, photo_count) 
        VALUES ('$name', '$description', curdate(), curdate(), 0, $p";
        $this->connection->query($sql);
        $album = $this->connection->insert_id;

        $sql = "INSERT INTO user_albums (user_id, album_id) 
        VALUES ($user, $album)";
        $this->connection->query($sql);
    }

    public function delete_album($album) {
        $sql = "DELETE FROM albums WHERE id = $album";
        $this->connection->query($sql);

        $sql = "DELETE FROM album_photos WHERE album_id = $album";
        $this->connection->query($sql);
    }

    public function get_albums($user) {
        $sql = "SELECT album_id, name, description, creation_date, update_date, public, photo_count 
        FROM user_albums
        INNER JOIN albums
        ON user_albums.album_id = albums.id
        WHERE user_id = $user";
        $albums = [];
        $data = $this->connection->query($sql);
        while($row = $data->fetch_assoc()) array_push($albums, $row);
        return $albums;
    }

    public function get_photos_in_public_album($user) {

        $sql = "SELECT album_id
        FROM user_albums
        INNER JOIN albums
        ON user_albums.album_id = albums.id
        WHERE user_id = $user AND public = 1";

        $album = $data = $this->connection->query($sql)->fetch_assoc()["album_id"];

        $sql = "SELECT photo_id, input_name, file_path, upload_date 
        FROM album_photos 
        INNER JOIN photos 
        ON album_photos.photo_id = photos.id 
        WHERE album_id = $album";
        $photos = [];
        $data = $this->connection->query($sql);
        while($row = $data->fetch_assoc()) array_push($photos, $row); 
        return $photos;
    }

    public function get_photos_in_album($album) {
        $sql = "SELECT photo_id, input_name, file_path, upload_date 
        FROM album_photos 
        INNER JOIN photos 
        ON album_photos.photo_id = photos.id 
        WHERE album_id = $album";
        $photos = [];
        $data = $this->connection->query($sql);
        while($row = $data->fetch_assoc()) array_push($photos, $row); 
        return $photos;
    }

    public function get_last_user_photo($user, $daysAgo) {
        $albums = $this->get_albums($user);
        $results = [];
        foreach($albums as $album) {
            $i = $album["album_id"];
            $sql = "SELECT photo_id, input_name, file_path, upload_date 
            FROM album_photos 
            INNER JOIN photos 
            ON album_photos.photo_id = photos.id 
            WHERE album_id = $i AND upload_date >= curdate() - interval $daysAgo day
            ORDER BY upload_date";
            $result = $this->connection->query($sql);
            if($result != "error"){ 
                while($row = $result->fetch_assoc()) 
                array_push($results, $row);
            }
        }
        return $results;
    }
//--------------------------------------------------------------------------------------------------------------------
//Робота з фотографіями
//--------------------------------------------------------------------------------------------------------------------
    public function add_photo($photo_info){
        extract($photo_info);
        $sql = "INSERT INTO photos (input_name, file_path, md5_hash, upload_date, tags) 
        VALUES ('$name', '$path', '$hash', curdate(), '$tags')";
        $this->connection->query($sql);
        return mysqli_insert_id($this->connection);
    }

    public function delete_photo($photo) {
        $sql = "DELETE FROM photos WHERE id = '$photo'";
        $this->connection->query($sql);

        $sql = "DELETE FROM album_photos WHERE photo_id = '$photo'";
        $this->connection->query($sql);
    }

//--------------------------------------------------------------------------------------------------------------------
//Робота з профілем користувача
//--------------------------------------------------------------------------------------------------------------------
    public function get_user_albums($user) {
        $sql = "SELECT album_id, name, description, public, photo_count, creation_date, update_date 
        FROM user_albums 
        INNER JOIN albums 
        ON albums.id = user_albums.album_id WHERE user_id = $user";
        return $this->connection->query($sql)->fetch_assoc();
    }

    public function change_avatar($user, $avatar) {

    }

    public function update_user_password($user, $password) {
        $sql = "UPDATE users SET password = '$password'
        WHERE users.id = $user";
    }

    public function update_user_info($user, $nick, $email) {
        $sql = "UPDATE users SET nick = '$nick', email = '$email'
        WHERE users.id = $user";
    }

    public function add_comment($user, $photo, $comment) {
        $sql = "INSERT INTO comments (user, photo_id, comment, add_date) 
        VALUES ($user, $photo, '$comment', curdate())";
        $this->connection->query($sql);
    }

    public function get_photo_comments($photo) {
        $sql = "SELECT nick, comment, add_date 
                FROM comments
                INNER JOIN users
                ON user = users.id 
                WHERE photo_id = $photo
                ORDER BY add_date DESC";
        $comments = [];
        $data = $this->connection->query($sql);
        while($row = $data->fetch_assoc()) array_push($comments, $row); 
        return $comments;
    }

    public function get_user_subs($user) {
        $sql = "SELECT subscribe_id, nick, avatar 
        FROM subscribe
        INNER JOIN users 
        ON users.id = subscribe.subscribe_id 
        WHERE user_id = $user";
        $subs = [];
        $data = $this->connection->query($sql);
        while($row = $data->fetch_assoc()) array_push($subs, $row); 
        return $subs;
    }

    public function get_user_info($user) {
        $sql = "SELECT * FROM users WHERE id = $user";
        return $this->connection->query($sql)->fetch_assoc();
    }
//--------------------------------------------------------------------------------------------------------------------

}

?>