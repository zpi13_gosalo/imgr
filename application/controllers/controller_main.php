<?php
    class Controller_Main extends Controller
    {
        function __construct()
        {
            $this->model = new Model_Main();
            $this->view = new View();
        }

        function action_index()
        {
            session_start();
            $this->view->generate('', 'main_view.php', $data);
        }

        function action_check_user()
        {
            session_start();
            if (isset($_SESSION["username"])) echo "ok";
        }

        function action_authorize()
        {
            session_start();
            $login = $_POST['username'];
            $password = $_POST['password'];

            if(!$user_db = $this->model->try_authorize($login, $password)) { 
                session_destroy(); 
                $resp["status"] = "error";
                echo json_encode($resp);
            } 
            else { 
                    $_SESSION["username"] = $login;
                    $user["id"] = $user_db["id"];
                    $user["name"] = $user_db["username"];
                    $user["avatar"] = $user_db["avatar"];
                    $user["nick"] = $user_db["nick"];
                    $user["email"] = $user_db["email"];
                    $user["status"] = "ok";
                    
                    echo json_encode($user); 
            }
        }

        function action_add_comment()
        {
            $user = $_POST["user"];
            $photo = $_POST["photo"];
            $comment = $_POST["comment"];

            $this->model->add_comment($user, $photo, $comment);
        }

        function action_user_info()
        {
            $type = $_POST["type"];

            $user = $_POST["user"];
            $nick = $_post["nick"];
            $email = $_post["email"];

            $avatar = $_POST["avatar"];

            $password = $_POST["password"];
            $retype = $_POST["retype"];

            switch($type)
            {
                case "info":
                    $this->model->update_user_info($user, $nick, $email); 
                break;
                case "avatar": 
                    $this->model->change_avatar($user, $avatar);
                break;
                case "password":
                    if($password == $retype) 
                        $this->model->update_user_password($user, $password);
                break;
            }
        }

        function action_get_comments()
        {
            $photo = $_POST["photo"];

            $comments = $this->model->get_photo_comments($photo);
            echo json_encode($comments);
        }


        function action_get_subs()
        {
            $user = $_POST["user"];
            $subs = $this->model->get_user_subs($user);

            echo json_encode($subs);
        }

        function action_register()
        {
            $username = $_POST["username"];
            $password = $_POST["password"];
            $retype = $_POST["password_retype"];
            $email = $_POST["email"];
            $accept = $_POST["accept_terms"];

            if($accept == 1)  
            if($password == $retype) 
            {
                $this->model->register($username, $password, $email);
            }
            
        }

        function action_logout()
        {
            session_start();
            session_destroy();
        }
    }
?>