<?php
    class Controller_Photo extends Controller
    {
        function __construct()
        {
            $this->model = new Model_Main();
        }

        function action_index()
        {
        }

        function action_upload()
        {
            ini_set('max_execution_time', 120);
            $destination = realpath('./imgs');

            $album = $_POST["album"];

            if (isset($_FILES['upload'])){
                $file = $_FILES['upload'];

                $ext = pathinfo($file["name"], PATHINFO_EXTENSION);
                $name = pathinfo($file["name"], PATHINFO_FILENAME);
                $sname = md5($file["name"]).".".$ext;
                $hash = md5($file["name"]);
                $filename = $destination."/".preg_replace("|[\\\/]|", "", $sname);

                $photo_info["name"] = $name;
                $photo_info["path"] = $sname;
                $photo_info["hash"] = $hash;
                $photo_info["tags"] = $tags;

                if ($filename !== "" && !file_exists($filename)){
                    move_uploaded_file($file["tmp_name"], $filename);
                    $photo = $this->model->add_photo($photo_info);
                    $this->model->add_photo_to_album($photo, $album);
                    $res = array("status" => "server");
                } else {
                    $res = array("status" => "error");
                }
                echo json_encode($res);
            }

        }


        function action_delete()
        {
            $photo = $_POST["photo"];
            $this->model->delete_photo($photo);
        }

        function action_download()
        {
            $action = $_POST["action"];
            $user = $_POST["user"];
            $album = $_POST["album"];

            $photos = null;
            switch($action)
            {
                case "get_last":
                    $photos = $this->model->get_last_user_photo($user, 10);
                    break;
                case "get_album":
                    $photos = $this->model->get_photos_in_album($album);
                    break;
                case "get_public":
                    $photos = $this->model->get_photos_in_public_album($user);
                    break;
            }

            if($photos != null) echo json_encode($photos);
            else echo json_encode($photos["status"] = "error");
        }
    }
?>