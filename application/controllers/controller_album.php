<?php
    class Controller_Album extends Controller
    {
        function __construct()
        {
            $this->model = new Model_Main();
            $this->view = new View();
        }

        function check_auth()
        {
            session_start();
            if(isset($_SESSION["username"])) { session_destroy(); die(); } 
        }

        function action_index()
        {
        }

        function action_get_albums()
        {
            $user = $_POST['user_id'];
            $albums = $this->model->get_albums($user);
            echo json_encode($albums);
        }

        function action_create()
        {
        }

        function action_delete()
        {
        }

        function action_add_photo()
        {
        }

        function action_remove_photo()
        {
        }
    }
?>